#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals


# C. Сортировка по последнему числу
# Дан спискок непустых списков.
# Нужно вернуть список, отсортированный по
# возрастанию последнего элемента каждого подсписка.
# Например: из [[1, 7], [1, 3], [3, 4, 5], [2, 2]] получится
# [[2, 2], [1, 3], [3, 4, 5], [1, 7]]
# Подсказка: используйте параметр key= функции сортировки,
# чтобы получить последний элемент подсписка.

def sort_last(lists):
    def sort_by_e(last):
        return last[-1]
    sor = sorted(lists, key=sort_by_e)
    print sor

sort_last([[1, 3], [3, 2], [2, 1]])

print 'next task'
# D. Удаление соседей
# Дан список чисел.
# Нужно вернуть список, где все соседние элементы
# были бы сведены к одному элементу.
# Таким образом, из [1, 2, 2, 3, 4, 4] получится [1, 2, 3, 4].
def remove_adjacent(nums):
    new = []
    old = None
    for x in nums:
        if x != old:
            new.append(x)
        old = x

    print new
    # print old
remove_adjacent([1, 2, 2, 3])
remove_adjacent([2, 2, 3, 3, 3, 3])
